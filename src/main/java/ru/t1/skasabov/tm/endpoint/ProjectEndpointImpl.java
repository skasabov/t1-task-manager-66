package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.ProjectRestEndpoint;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public final class ProjectEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public ProjectDto save(@NotNull @RequestBody final ProjectDto project) {
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskRepository.deleteByProjectId(id);
        projectRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final ProjectDto project) {
        taskRepository.deleteByProjectId(project.getId());
        projectRepository.delete(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<ProjectDto> projects) {
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByProjectId(project.getId());
        projectRepository.deleteAll(projects);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        @NotNull final List<ProjectDto> projects = projectRepository.findAll();
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByProjectId(project.getId());
        projectRepository.deleteAll();
    }

}
