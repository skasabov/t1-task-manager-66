package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectRestEndpoint {

    @NotNull
    List<ProjectDto> findAll();

    @NotNull
    ProjectDto save(ProjectDto project);

    @Nullable
    ProjectDto findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull ProjectDto project);

    void clear(@NotNull List<ProjectDto> projects);

    void clear();

}
