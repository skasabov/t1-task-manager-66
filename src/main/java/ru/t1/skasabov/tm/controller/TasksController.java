package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

@Controller
public final class TasksController {

    @Autowired
    private TaskDtoRepository taskRepository;

    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task-list", "tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}
