package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.TaskRestEndpointClient;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRestEndpointClientTest {

    private final static int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    @Before
    public void initTest() {
        taskList = client.findAll();
        client.clear();
        client.save(task1);
        client.save(task2);
        client.save(task3);
        client.save(task4);
    }

    @After
    public void clean() {
        client.clear();
        for (@NotNull final TaskDto task : taskList) client.save(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindAll() {
        @NotNull final List<TaskDto> tasks = client.findAll();
        Assert.assertEquals(NUMBER_OF_ENTITIES, tasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testSave() {
        client.save(new TaskDto("Test Task"));
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindById() {
        @Nullable final TaskDto Task = client.findById(task1.getId());
        Assert.assertNotNull(Task);
        Assert.assertEquals("Test Task 1", Task.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindByInvalidId() {
        @Nullable final TaskDto task = client.findById("123");
        Assert.assertNull(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsById() {
        boolean existsTask = client.existsById(task1.getId());
        Assert.assertTrue(existsTask);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsByInvalidId() {
        boolean existsTask = client.existsById("123");
        Assert.assertFalse(existsTask);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCount() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteById() {
        client.deleteById(task1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDelete() {
        client.delete(task1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testClearTasks() {
        @NotNull final List<TaskDto> Tasks = new ArrayList<>();
        Tasks.add(task2);
        Tasks.add(task3);
        Tasks.add(task4);
        client.clear(Tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES - Tasks.size(), client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testClearEmptyTasks() {
        client.clear(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testClear() {
        client.clear();
        Assert.assertEquals(0, client.count());
    }
    
}
